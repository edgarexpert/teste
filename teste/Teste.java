package teste;

import teste.model.Caminhao;
import teste.model.Motorista;
import teste.model.Seguro;
import teste.repository.Motoristas;

import java.math.BigDecimal;
import java.util.Optional;

class Teste {
    public static void main(String[] args) {
        Motoristas motoristas = new Motoristas();

        //Optional motorista = motoristas.porNome("João");

        //String cobertura = motorista.getCaminhao().getSeguro().getCobertura();
        //String cobertura = cobertura != null ? cobertura : "Sem seguro";

        //motorista = motoristas.porNome("João");
        //cobertura = "Sem seguro";

        //System.out.println(cobertura);

        /*if (motorista != null) {
            Caminhao caminhao = motorista.getCaminhao();
            if (caminhao != null) {
                Optional seguro = caminhao.getSeguro();
                if (seguro != null) {
                    cobertura = seguro.getCobertura();
                }
            }
        }*/

        //System.out.println(cobertura);

        String cobertura = motoristas.porNome("José")
                .flatMap(Motorista::getCaminhao)
                .flatMap(Caminhao::getSeguro)
                .map(Seguro::getCobertura)
                .orElse("Sem cobertua");

        System.out.println("A cobertura é: " + cobertura);

        Seguro seguro = new Seguro("Total com franquia reduzida", new BigDecimal("600"));
        Optional<Seguro>  seguroOpcional = Optional.of(seguro);
        System.out.println(seguroOpcional);

        seguro = null;
        seguroOpcional = Optional.ofNullable(seguro);
        System.out.println(seguroOpcional);

        seguroOpcional.map(Seguro::getValorFranquia).ifPresent(System.out::println);

        //Optional caminhaoOpcional = motoristas.porNome("João")
        //.map(Motorista::getCaminhao);

        Optional caminhaoOpcional = motoristas.porNome("João")
                .flatMap(Motorista::getCaminhao);

    }
}